package com.cibertect.entity;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name="tb_medicamento")
public class Medicamento {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_med")
	private int codMedicamento;
	@Column(name="nom_med")
	private String nomMedicamento;
	@Column(name="stock_med")
	private int stockMedicamento;
	@Column(name="pre_med")
	private double precioMedicamento;
	//@Temporal(TemporalType.DATE)
	@Column(name="fec_ven_med")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es-PE", timezone = "America/Lima")
	private Date fechaMedicamento;
	//
	@ManyToOne
	@JoinColumn(name="cod_lab")
	private Laboratorio laboratorio;
	
	public Medicamento(){
		laboratorio=new Laboratorio();
	}
	public int getCodMedicamento() {
		return codMedicamento;
	}
	public void setCodMedicamento(int codMedicamento) {
		this.codMedicamento = codMedicamento;
	}
	public String getNomMedicamento() {
		return nomMedicamento;
	}
	public void setNomMedicamento(String nomMedicamento) {
		this.nomMedicamento = nomMedicamento;
	}
	public int getStockMedicamento() {
		return stockMedicamento;
	}
	public void setStockMedicamento(int stockMedicamento) {
		this.stockMedicamento = stockMedicamento;
	}
	public double getPrecioMedicamento() {
		return precioMedicamento;
	}
	public void setPrecioMedicamento(double precioMedicamento) {
		this.precioMedicamento = precioMedicamento;
	}
	public Date getFechaMedicamento() {
		return fechaMedicamento;
	}
	public void setFechaMedicamento(Date fechaMedicamento) {
		this.fechaMedicamento = fechaMedicamento;
	}
	public Laboratorio getLaboratorio() {
		return laboratorio;
	}
	public void setLaboratorio(Laboratorio laboratorio) {
		this.laboratorio = laboratorio;
	}
	
}
