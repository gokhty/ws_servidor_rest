package com.cibertect.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cibertect.entity.Laboratorio;
import com.cibertect.repository.LaboratorioDAO;
@Service
public class LaboratorioService {
	@Autowired
	private LaboratorioDAO dao;

	public List<Laboratorio> lista(){
		return dao.findAll();
	}
	public void save(Laboratorio bean) {
		dao.save(bean);
	}
	public void delete(Integer cod) {
		dao.deleteById(cod);
	}
	
	
	
}



