package com.cibertect.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cibertect.entity.Medicamento;
import com.cibertect.repository.MedicamentoDAO;
@Service
public class MedicamentoService {
	@Autowired
	private MedicamentoDAO daoMediamento;
	
	public List<Medicamento> listAll(){
		return daoMediamento.findAll();
	}
	public void save(Medicamento bean) {
		daoMediamento.save(bean);
	}
	public void update(Medicamento bean) {
		daoMediamento.save(bean);
	}
	public void delete(Integer cod) {
		daoMediamento.deleteById(cod);
	}
	
	public Optional<Medicamento> buscarPorCodgo(Integer cod) {	
		return daoMediamento.findById(cod);
	}

	
	/*public void buscarPorNombre(String nom) {
		daoMediamento.findOne(nom);
	}*/
	
}


