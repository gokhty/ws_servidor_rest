package com.cibertect.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.cibertect.entity.Laboratorio;
@Repository
public interface LaboratorioDAO extends JpaRepository<Laboratorio, Integer>{
	
}

