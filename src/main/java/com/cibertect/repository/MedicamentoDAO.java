package com.cibertect.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.cibertect.entity.Medicamento;
public interface MedicamentoDAO extends JpaRepository<Medicamento, Integer>{

}

