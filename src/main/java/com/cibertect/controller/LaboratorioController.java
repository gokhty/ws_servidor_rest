package com.cibertect.controller;
import java.util.List;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cibertect.entity.Laboratorio;
import com.cibertect.service.LaboratorioService;

@RestController
@RequestMapping(value = "/laboratorio")
public class LaboratorioController {
	@Autowired
	private LaboratorioService service;
	
	
	@ResponseBody
	//@GetMapping(path = "lista", produces =  "application/json")
	@GetMapping(path = "lista", produces =  MediaType.APPLICATION_JSON_VALUE)
	public List<Laboratorio> lista() {
		return service.lista();
	}
	@PostMapping(path = "/registra")
	public void registra(@RequestBody Laboratorio bean) {
		 service.save(bean);
	}
	@PutMapping(path = "/actualiza")
	public void actualiza(@RequestBody Laboratorio bean) {
		 service.save(bean);
	}
	
	@DeleteMapping(path = "/elimina/{codigo}")
	public void eliminar(@PathVariable("codigo") Integer cod) {
		service.delete(cod);
	}
}
